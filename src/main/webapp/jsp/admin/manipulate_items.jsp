<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  Created by IntelliJ IDEA.
  User: shadman
  Date: 11/22/17
  Time: 9:17 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Manipulate Items</title>
</head>

<body>
<c:import url="../private/common_components.jsp"/>

<c:import url="../private/items.jsp"/>

<h3>Create Item</h3>

<form action="${pageContext.request.contextPath}/CreateItemServlet" method="post">
    Item Name: <input type="text" name="itemName" required>
    <input type="submit" value="Create Item">
</form>

<h3>Update Item</h3>

<form action="${pageContext.request.contextPath}/UpdateItemServlet" method="post">
    <select name="itemId">
        <c:forEach items="${applicationScope.items}" var="item">
            <option value="${item.id}"><c:out value="${item.name}"/></option>
        </c:forEach>
    </select>
    <input type="text" name="newItemName" placeholder="type new name here" required>
    <input type="submit" value="Update">
</form>

<h3>Delete Item</h3>

<form action="${pageContext.request.contextPath}/DeleteItemServlet" method="post">
    <select name="itemId">
        <c:forEach items="${applicationScope.items}" var="item">
            <option value="${item.id}"><c:out value="${item.name}"/></option>
        </c:forEach>
    </select>
    <input type="submit" value="Delete">
</form>
</body>
</html>
