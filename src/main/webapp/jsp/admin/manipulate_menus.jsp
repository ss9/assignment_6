<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  Created by IntelliJ IDEA.
  User: shadman
  Date: 11/22/17
  Time: 9:17 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Manipulate Menus</title>
</head>

<body>
<c:import url="../private/common_components.jsp"/>

<c:import url="../private/menus.jsp"/>

<h3>Create/Update Menu</h3>
<h4>Select the day, serving type and items of the menu</h4>

<form action="${pageContext.request.contextPath}/CreateMenuServlet" method="post">
    <select name="createMenuDay">
        <c:forEach items="${applicationScope.days}" var="day">
            <option value="${day}"><c:out value="${applicationScope.intToDay[day]}"/></option>
        </c:forEach>
    </select>
    <select name="createMenuServingType">
        <c:forEach items="${applicationScope.servingTypes}" var="servingType">
            <option value="${servingType}"><c:out value="${applicationScope.intToServingType[servingType]}"/></option>
        </c:forEach>
    </select><br><br>
    <c:forEach items="${applicationScope.items}" var="item">
        <input type="checkbox" name="createMenuItem" value="${item.id}">
        <c:out value="${item.name}"/>
    </c:forEach>
    <br><br>
    <input type="submit" value="Create/Update Menu">
</form>

<h3>Delete Menu</h3>
<h4>Select the day and serving type of the menu to be deleted</h4>

<form action="${pageContext.request.contextPath}/DeleteMenuServlet" method="post">
    <select name="deleteMenuDay">
        <c:forEach items="${applicationScope.days}" var="day">
            <option value="${day}"><c:out value="${applicationScope.intToDay[day]}"/></option>
        </c:forEach>
    </select>
    <select name="deleteMenuServingType">
        <c:forEach items="${applicationScope.servingTypes}" var="servingType">
            <option value="${servingType}"><c:out value="${applicationScope.intToServingType[servingType]}"/></option>
        </c:forEach>
    </select>
    <br><br>
    <input type="submit" value="Delete Menu">
</form>
</body>
</html>
