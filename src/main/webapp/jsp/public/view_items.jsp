<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  Created by IntelliJ IDEA.
  User: shadman
  Date: 11/22/17
  Time: 9:17 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>View Items</title>
</head>

<body>
<c:import url="../private/common_components.jsp"/>
<c:import url="../private/items.jsp"/>
</body>
</html>
