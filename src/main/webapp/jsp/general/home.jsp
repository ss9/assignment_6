<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  Created by IntelliJ IDEA.
  User: shadman
  Date: 11/22/17
  Time: 9:17 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home</title>
</head>

<body>
<c:import url="../private/common_components.jsp"/>

<c:if test="${sessionScope.user.type=='admin'}">
    <h2><a href="${pageContext.request.contextPath}/jsp/admin/manipulate_items.jsp">Create/Update/Delete Items</a></h2>

    <h2><a href="${pageContext.request.contextPath}/jsp/admin/manipulate_menus.jsp">Create/Update/Delete Menus</a></h2>
</c:if>

<c:if test="${sessionScope.user.type=='public'}">
    <h2><a href="${pageContext.request.contextPath}/jsp/public/view_items.jsp">View Items</a></h2>

    <h2><a href="${pageContext.request.contextPath}/jsp/public/view_menus.jsp">View Menus</a></h2>
</c:if>
</body>
</html>
