<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  Created by IntelliJ IDEA.
  User: shadman
  Date: 11/22/17
  Time: 9:17 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Common Components</title>
</head>

<body>
<h1>Welcome <c:out value="${sessionScope.user.username}"/></h1>

<form action="${pageContext.request.contextPath}/LogoutServlet" method="post">
    <input type="submit" value="Logout">
</form>

<h2><a href="${pageContext.request.contextPath}/index.jsp">Home</a></h2>
</body>
</html>
