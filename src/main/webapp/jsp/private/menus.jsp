<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  Created by IntelliJ IDEA.
  User: shadman
  Date: 11/22/17
  Time: 9:17 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Menus</title>
</head>

<body>
<h3>Current Menu</h3>
<c:forEach items="${applicationScope.menu}" var="menu">
    <c:if test="${menu!=null}">
        <c:out value="${applicationScope.intToDay[menu.day]}"/>
        <c:out value="${applicationScope.intToServingType[menu.servingType]}"/>
        <ol>
            <c:forEach items="${menu.itemSet}" var="item">
                <li><c:out value="${item.name}"/></li>
            </c:forEach>
        </ol>
    </c:if>
</c:forEach>
</body>
</html>
