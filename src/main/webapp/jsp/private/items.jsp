<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  Created by IntelliJ IDEA.
  User: shadman
  Date: 11/22/17
  Time: 9:17 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Items</title>
</head>

<body>
<h3>Current items</h3>
<ol>
    <c:forEach items="${applicationScope.items}" var="item">
        <li><c:out value="${item.name}"/></li>
    </c:forEach>
</ol>
</body>
</html>
