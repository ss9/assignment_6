package net.therap.helper;

import net.therap.domain.Item;
import net.therap.domain.Menu;
import net.therap.domain.User;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 * @author shadman
 * @since 11/26/17
 */
public class HibernateUtil {
    public static SessionFactory getSessionFactory() {
        Configuration configuration = new Configuration();

        configuration.configure().addAnnotatedClass(User.class);
        configuration.configure().addAnnotatedClass(Item.class);
        configuration.configure().addAnnotatedClass(Menu.class);

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().
                applySettings(configuration.getProperties()).build();

        return configuration.buildSessionFactory(serviceRegistry);
    }
}
