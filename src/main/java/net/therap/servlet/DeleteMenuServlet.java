package net.therap.servlet;

import net.therap.dao.ItemDao;
import net.therap.dao.MenuDao;
import net.therap.domain.Menu;
import net.therap.helper.Helper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author shadman
 * @since 11/27/17
 */

@WebServlet(urlPatterns = {"/DeleteMenuServlet"})
public class DeleteMenuServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ItemDao itemDao = new ItemDao();
        MenuDao menuDao = new MenuDao();
        int day = Integer.parseInt(request.getParameter("deleteMenuDay"));
        int servingType = Integer.parseInt(request.getParameter("deleteMenuServingType"));
        Menu menu = menuDao.getMenuFromDayAndServingType(day, servingType);
        if (menu != null) {
            menuDao.deleteMenu(menu);
        }
        getServletContext().setAttribute("items", itemDao.getAllItems());
        getServletContext().setAttribute("menu", menuDao.getWeeklyMenu());
        response.sendRedirect(request.getContextPath() + Helper.MANIPULATE_MENUS_URL);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect(request.getContextPath() + Helper.INDEX_URL);
    }
}
