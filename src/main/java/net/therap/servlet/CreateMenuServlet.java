package net.therap.servlet;

import net.therap.dao.ItemDao;
import net.therap.dao.MenuDao;
import net.therap.domain.Item;
import net.therap.domain.Menu;
import net.therap.helper.Helper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * @author shadman
 * @since 11/26/17
 */

@WebServlet(urlPatterns = {"/CreateMenuServlet"})
public class CreateMenuServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ItemDao itemDao = new ItemDao();
        MenuDao menuDao = new MenuDao();
        int day = Integer.parseInt(request.getParameter("createMenuDay"));
        int servingType = Integer.parseInt(request.getParameter("createMenuServingType"));
        String[] items = request.getParameterValues("createMenuItem");
        if (items != null) {
            Set<Item> itemSet = new HashSet<>();
            for (String itemIdString : items) {
                int itemId = Integer.parseInt(itemIdString);
                Item item = itemDao.getItemById(itemId);
                itemSet.add(item);
            }
            if (menuDao.getMenuFromDayAndServingType(day, servingType) == null) {
                menuDao.addMenu(new Menu(day, itemSet, servingType));
            } else {
                Menu menu = menuDao.getMenuFromDayAndServingType(day, servingType);
                menuDao.updateMenu(menu, itemSet);
            }
            getServletContext().setAttribute("items", itemDao.getAllItems());
            getServletContext().setAttribute("menu", menuDao.getWeeklyMenu());
        }
        response.sendRedirect(request.getContextPath() + Helper.MANIPULATE_MENUS_URL);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect(request.getContextPath() + Helper.INDEX_URL);
    }
}
