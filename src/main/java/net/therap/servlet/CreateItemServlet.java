package net.therap.servlet;

import net.therap.dao.ItemDao;
import net.therap.dao.MenuDao;
import net.therap.domain.Item;
import net.therap.helper.Helper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author shadman
 * @since 11/26/17
 */

@WebServlet(urlPatterns = {"/CreateItemServlet"})
public class CreateItemServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String itemName = request.getParameter("itemName");
        Item item = new Item(itemName);
        ItemDao itemDao = new ItemDao();
        MenuDao menuDao = new MenuDao();
        itemDao.addItem(item);
        getServletContext().setAttribute("items", itemDao.getAllItems());
        getServletContext().setAttribute("menu", menuDao.getWeeklyMenu());
        response.sendRedirect(request.getContextPath() + Helper.MANIPULATE_ITEMS_URL);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect(request.getContextPath() + Helper.INDEX_URL);
    }
}
