package net.therap.servlet;

import net.therap.dao.ItemDao;
import net.therap.dao.MenuDao;
import net.therap.dao.UserDao;
import net.therap.domain.User;
import net.therap.helper.Helper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author shadman
 * @since 11/26/17
 */

@WebServlet(urlPatterns = {"/LoginServlet"})
public class LoginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        UserDao userDao = new UserDao();
        User user = userDao.getUserByUsername(username);
        if (user == null) {
            response.setContentType(Helper.CONTENT_TYPE_HTML);
            response.getWriter().write(Helper.ERROR_IN_USERNAME);
            response.getWriter().write("<br><a href=\"" + request.getContextPath() + "/index.jsp\"" + ">Home</a>");
        } else if (!user.getPassword().trim().equals(password)) {
            response.setContentType(Helper.CONTENT_TYPE_HTML);
            response.getWriter().write(Helper.ERROR_IN_PASSWORD);
            response.getWriter().write("<br><a href=\"" + request.getContextPath() + "/index.jsp\"" + ">Home</a>");
        } else {
            request.getSession().setAttribute("user", user);
            ItemDao itemDao = new ItemDao();
            MenuDao menuDao = new MenuDao();
            Helper helper = new Helper();
            getServletContext().setAttribute("intToDay", helper.INT_TO_DAY);
            getServletContext().setAttribute("days", helper.INT_TO_DAY.keySet());
            getServletContext().setAttribute("intToServingType", helper.INT_TO_SERVING_TYPE);
            getServletContext().setAttribute("servingTypes", helper.INT_TO_SERVING_TYPE.keySet());
            getServletContext().setAttribute("items", itemDao.getAllItems());
            getServletContext().setAttribute("menu", menuDao.getWeeklyMenu());
            response.sendRedirect(request.getContextPath() + Helper.HOME_URL);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect(request.getContextPath() + Helper.INDEX_URL);
    }
}
