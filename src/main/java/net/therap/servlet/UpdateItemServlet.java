package net.therap.servlet;

import net.therap.dao.ItemDao;
import net.therap.dao.MenuDao;
import net.therap.domain.Item;
import net.therap.helper.Helper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author shadman
 * @since 11/26/17
 */

@WebServlet(urlPatterns = {"/UpdateItemServlet"})
public class UpdateItemServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String itemIdString = request.getParameter("itemId");
        if (itemIdString != null) {
            int itemId = Integer.parseInt(itemIdString);
            String newItemName = request.getParameter("newItemName");
            ItemDao itemDao = new ItemDao();
            MenuDao menuDao = new MenuDao();
            Item item = itemDao.getItemById(itemId);
            itemDao.updateItem(item, newItemName);
            getServletContext().setAttribute("items", itemDao.getAllItems());
            getServletContext().setAttribute("menu", menuDao.getWeeklyMenu());
        }
        response.sendRedirect(request.getContextPath() + Helper.MANIPULATE_ITEMS_URL);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect(request.getContextPath() + Helper.INDEX_URL);
    }
}
