package net.therap.filter;

import net.therap.domain.User;
import net.therap.helper.Helper;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author shadman
 * @since 11/26/17
 */

@WebFilter(urlPatterns = {"/jsp/*"})
public class LoginFilter implements Filter {

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        if (request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
            HttpServletRequest httpServletRequest = (HttpServletRequest) request;
            HttpServletResponse httpServletResponse = (HttpServletResponse) response;
            User user = (User) httpServletRequest.getSession().getAttribute("user");
            if (user == null) {
                httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/LoginServlet");
            } else {
                if (!user.getType().trim().equals(Helper.USER_TYPE_ADMIN) &&
                        httpServletRequest.getRequestURL().toString().contains("/admin/")) {
                    httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + Helper.INDEX_URL);
                }
                if (!user.getType().trim().equals(Helper.USER_TYPE_PUBLIC) &&
                        httpServletRequest.getRequestURL().toString().contains("/public/")) {
                    httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + Helper.INDEX_URL);
                }
                filterChain.doFilter(request, response);
            }
        }
    }

    public void destroy() {

    }

    public void init(FilterConfig filterConfig) throws ServletException {

    }
}
