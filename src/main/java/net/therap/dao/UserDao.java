package net.therap.dao;

import net.therap.domain.User;
import net.therap.helper.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * @author shadman
 * @since 11/26/17
 */
public class UserDao {

    private SessionFactory sessionFactory;

    public UserDao() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }

    public User getUserByUsername(String username) {
        User user;
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        user = (User) session.get(User.class, username);
        session.getTransaction().commit();
        return user;
    }

    public void addUser(User user) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.save(user);
        session.getTransaction().commit();
    }
}
