package net.therap.dao;

import net.therap.domain.Item;
import net.therap.helper.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * @author shadman
 * @since 11/26/17
 */
public class ItemDao {

    private SessionFactory sessionFactory;

    public ItemDao() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }

    public void addItem(Item item) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.save(item);
        session.getTransaction().commit();
    }

    public Item getItemById(int id) {
        Item item = null;
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        item = (Item) session.get(Item.class, id);
        session.getTransaction().commit();
        return item;
    }

    public void updateItem(Item item, String newItemName) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        item.setName(newItemName);
        session.update(item);
        session.getTransaction().commit();
    }

    public void deleteItem(Item item) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.delete(item);
        session.getTransaction().commit();
    }

    public List<Item> getAllItems() {
        List<Item> itemList;
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        Query query = session.createQuery("from net.therap.domain.Item");
        itemList = query.list();
        session.getTransaction().commit();
        return itemList;
    }
}
