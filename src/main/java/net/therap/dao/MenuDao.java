package net.therap.dao;

import net.therap.domain.Item;
import net.therap.domain.Menu;
import net.therap.domain.MenuPrimaryKey;
import net.therap.helper.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;
import java.util.Set;

/**
 * @author shadman
 * @since 11/26/17
 */
public class MenuDao {

    private SessionFactory sessionFactory;

    public MenuDao() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }

    public void deleteMenu(Menu menu) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.delete(menu);
        session.getTransaction().commit();
    }

    public void addMenu(Menu menu) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.save(menu);
        session.getTransaction().commit();
    }

    public Menu getMenuFromDayAndServingType(int day, int servingType) {
        Menu menu;
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        MenuPrimaryKey menuPrimaryKey = new MenuPrimaryKey(day, servingType);
        menu = (Menu) session.get(Menu.class, menuPrimaryKey);
        session.getTransaction().commit();
        return menu;
    }

    public List<Menu> getWeeklyMenu() {
        List<Menu> menuList;
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM Menu");
        menuList = query.list();
        session.getTransaction().commit();
        return menuList;
    }

    public void updateMenu(Menu menu, Set<Item> itemSet) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        menu.setItemSet(itemSet);
        session.update(menu);
        session.getTransaction().commit();
    }
}
